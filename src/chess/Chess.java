package chess;

import java.util.ArrayList;
import java.util.Scanner;

import board.Board;

import pieces.*;

import pieces.Color;
import pieces.King;
import pieces.Pawn;
import pieces.Piece;

/**
 * Main Method that takes in input and starts the game
 * manipulates the board
 * @author Kenny Salanga, Kelli Jose
 */
public class Chess {

	/**
	 * main method to start the chess game
	 * @param args
	 */
    public static void main(String args[]) {
    	Board board = new Board();
		Piece[][] pieceBoard = board.getBoard();
		System.out.println(board.printBoard());
		System.out.println();
		System.out.print("White's move: ");
        Scanner sc = new Scanner (System.in);
		String input = sc.nextLine();
		while(input != null) {
			String[]moveInfo = input.split(" ");
			if(input.equals("resign") == true) {
				if(board.getColorMove() == Color.BLACK) {
					System.out.print("White wins");}
				else {System.out.print("Black wins");}
				break;
			}
			if(moveInfo.length > 2) {
				if(moveInfo[2].equals("draw?") == true) {
					System.out.println("draw");
					break;}
			}
			String from = moveInfo[0];
			int[] fromCoords = Board.getCoordinates(from);
			Piece playingPiece = pieceBoard[fromCoords[0]][fromCoords[1]];

			String to = moveInfo[1];
			int[] toCoords = Board.getCoordinates(to);

			// Castling Check
			ArrayList<ArrayList<Integer>> allMoves = board.getAllMoves(board.getColorMove() == Color.WHITE ? Color.BLACK : Color.WHITE);
			if (board.getColorMove() == Color.WHITE) {
				((King)board.getKingWhite()).setEnemyMoves(allMoves);
			} else {
				((King)board.getKingBlack()).setEnemyMoves(allMoves);
			}

			if(playingPiece == null
				|| (playingPiece.getColor() == Color.WHITE && board.getColorMove() == Color.BLACK)
				|| (playingPiece.getColor() == Color.BLACK && board.getColorMove() == Color.WHITE)
				|| playingPiece.move(to, board) == false
			) {
				System.out.println("Illegal move, try again");
			} else {

				// Saves about to be taken piece
				Piece takenPiece = pieceBoard[toCoords[0]][toCoords[1]];

				if (playingPiece instanceof Pawn) {
					// En passant condition
					if (
						fromCoords[1] != toCoords[1] // captures diagonally
						&& pieceBoard[fromCoords[0]][toCoords[1]] != null // piece next to it is not null
						&& pieceBoard[fromCoords[0]][toCoords[1]] instanceof Pawn // piece next to it is pawn
						&& ((Pawn)pieceBoard[fromCoords[0]][toCoords[1]]).isEnPassant()
					) {
						// Updates piece to the one next to it rather than the space it actually takes
						takenPiece = pieceBoard[fromCoords[0]][toCoords[1]];

						// Removes enpassant pawn next to the playing piece
						board.setBoard(fromCoords[0], toCoords[1], null);
					}
				}

				// castling
				if(playingPiece instanceof King) {
					if(toCoords[1] - fromCoords[1] == 2
					&& ((King)playingPiece).isFirstMove()) { // right side castle

						// Move Rook to King's Left
						Rook rook = (Rook) pieceBoard[fromCoords[0]][7];
						board.setBoard(fromCoords[0], 7, null);

						board.setBoard(fromCoords[0], fromCoords[1] + 1, rook);

						rook.setRank(Board.rowToRank(fromCoords[0]));
						rook.setFile(Board.colToFile(fromCoords[1] + 1));
						rook.setCoordinates(new int[]{fromCoords[0], fromCoords[1] + 1});
						rook.setFirstMove(false);
					} else if (toCoords[1] - fromCoords[1] == -2
					&& ((King)playingPiece).isFirstMove()) { // left side castle

						// Move Rook to King's Right
						Rook rook = (Rook) pieceBoard[fromCoords[0]][0];
						board.setBoard(fromCoords[0], 0, null);

						board.setBoard(fromCoords[0], fromCoords[1] - 1, rook);
						rook.setRank(Board.rowToRank(fromCoords[0]));
						rook.setFile(Board.colToFile(fromCoords[1] - 1));
						rook.setCoordinates(new int[]{fromCoords[0], fromCoords[1] - 1});
						rook.setFirstMove(false);
					}
				}

				board.setBoard(fromCoords[0], fromCoords[1], null);
				playingPiece.setRank(Board.rowToRank(toCoords[0]));
				playingPiece.setFile(Board.colToFile(toCoords[1]));
				playingPiece.setCoordinates(toCoords);
				board.setBoard(toCoords[0], toCoords[1], playingPiece);

				// Friendly check
				if (board.kingInCheck(board.getColorMove())) {
					// Reverts piece
					board.setBoard(fromCoords[0], fromCoords[1], playingPiece);

					playingPiece.setRank(Board.rowToRank(fromCoords[0]));
					playingPiece.setFile(Board.colToFile(fromCoords[1]));
					playingPiece.setCoordinates(fromCoords);

					board.setBoard(toCoords[0], toCoords[1], null);
					if (takenPiece != null) {
						board.setBoard(takenPiece.getCoordinates()[0], takenPiece.getCoordinates()[1], takenPiece);
					}

					System.out.println("Illegal move, try again");
				} else { // Valid Move
					Color opposingKingColor = board.getColorMove() == Color.WHITE ? Color.BLACK : Color.WHITE;

					// First Move, EnPassant, and Pawn Promotion update
					if (playingPiece instanceof Pawn) {

						if (((Pawn)playingPiece).isFirstMove()) {
							((Pawn)playingPiece).setFirstMove(false);
							if (Math.abs(fromCoords[0] - toCoords[0]) == 2) {
								((Pawn)playingPiece).setEnpassant(true);
							}
						}

						int playingPieceRow = playingPiece.getCoordinates()[0];
						int playingPieceCol = playingPiece.getCoordinates()[1];

						char pieceName = moveInfo.length > 2 ? moveInfo[2].charAt(0) : 'Q';

						if (playingPieceRow == 7 || playingPieceRow == 0) { // on edge of board
							switch (pieceName) {
								case 'N':
									pieceBoard[playingPieceRow][playingPieceCol] = new Knight('N', playingPiece.getColor(), playingPiece.getFile(), playingPiece.getRank());
									break;
								case 'R':
									pieceBoard[playingPieceRow][playingPieceCol] = new Rook('R', playingPiece.getColor(), playingPiece.getFile(), playingPiece.getRank());
									break;
								case 'B':
									pieceBoard[playingPieceRow][playingPieceCol] = new Bishop('B', playingPiece.getColor(), playingPiece.getFile(), playingPiece.getRank());
									break;
								default:
									pieceBoard[playingPieceRow][playingPieceCol] = new Queen('Q', playingPiece.getColor(), playingPiece.getFile(), playingPiece.getRank());
							}
						}
					}

					if (playingPiece instanceof King) {
						((King)playingPiece).setFirstMove(false);
					}

					// Attacking Check
					if (board.kingInCheck(opposingKingColor)) {
						if (opposingKingColor == Color.WHITE) {
							((King)board.getKingWhite()).setEnemyMoves(board.getAllMoves(opposingKingColor));
						} else {
							((King)board.getKingBlack()).setEnemyMoves(board.getAllMoves(opposingKingColor));
						}
						// Calculate for Checkmate here
						if(board.kingInCheckmate(opposingKingColor)){
							System.out.println();
							System.out.println(board.printBoard());
							System.out.println("Checkmate");
							if (board.getColorMove() == Color.WHITE) {
								System.out.println("White Wins");
							} else {
								System.out.println("Black Wins");
							}
							break;
						} else {
							System.out.println("Check");
						}
					}

					board.switchColorMove();
					board.clearEnpassants(board.getColorMove());

					System.out.println();
					System.out.println(board.printBoard());
					System.out.println();
				}
			}

			if(board.getColorMove() == Color.BLACK) {
				System.out.print("Black's move: ");}
			else {System.out.print("White's move: ");}

			if(sc.hasNextLine()) {
				input = sc.nextLine();}
			else{input = null;}
		}
    }
}