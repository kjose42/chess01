package board;

import java.util.ArrayList;
import java.util.Arrays;

import pieces.*;

/**
 * Board is responsible for moving chess pieces and keeping track of whose turn it is
 * 
 * @author Kenny Salanga, Kelli Jose
 *
 */
public class Board {
	
	/** 
	 * 2D array that stores all chess pieces on the board.
	 * Chess pieces are represented by the Piece object.
	 * 
	 * @see Piece
	 */
	private Piece[][] chessBoard;
	
	/**
	 * Refers to the black King on the chess board.
	 * 
	 * @see King
	 */
	private Piece kingBlack;
	
	/**
	 * Refers to the white King on the chess board.
	 * 
	 * @see King
	 */
	private Piece kingWhite;
	
	/**
	 * Indicates which player's turn it is.
	 * (colorMove is either white or black).
	 * 
	 * @see Color
	 */
	private Color colorMove;
	
	/**
	 * Initializes chess Board.
	 * Places the pieces in their starting location in chessBoard.
	 * R = rook
	 * N = Knight
	 * B = Bishop
	 * Q = Queen
	 * K = King
	 * p = Pawn
	 * Only allows white to go first.
	 * Stores black King and white King in blackKing and whiteKing respectively.
	 * 
	 * @see Color
	 * @see Piece
	 */
	public Board(){
		colorMove = Color.WHITE;
		chessBoard = new Piece [8][8];
		chessBoard[0][0] = new Rook('R', Color.BLACK, 'a', 8);
		chessBoard[0][1] = new Knight('N', Color.BLACK, 'b', 8);
		chessBoard[0][2] = new Bishop('B', Color.BLACK, 'c', 8);
		chessBoard[0][3] = new Queen('Q', Color.BLACK, 'd', 8);
		chessBoard[0][4] = new King('K', Color.BLACK, 'e', 8);
		chessBoard[0][5] = new Bishop('B', Color.BLACK, 'f', 8);
		chessBoard[0][6] = new Knight('N', Color.BLACK, 'g', 8);
		chessBoard[0][7] = new Rook('R', Color.BLACK, 'h', 8);
		for(int inc = 0; inc < 8; inc++) {
			chessBoard[1][inc] = new Pawn('p', Color.BLACK, (char)('a' + inc), 7);
			chessBoard[6][inc] = new Pawn('p', Color.WHITE, (char)('a' + inc), 2);}
		chessBoard[7][0] = new Rook('R', Color.WHITE, 'a', 1);
		chessBoard[7][1] = new Knight('N', Color.WHITE, 'b', 1);
		chessBoard[7][2] =  new Bishop('B', Color.WHITE, 'c', 1);
		chessBoard[7][3] = new Queen('Q', Color.WHITE, 'd', 1);
		chessBoard[7][4] = new King('K', Color.WHITE, 'e', 1);
		chessBoard[7][5] = new Bishop('B', Color.WHITE, 'f', 1);
		chessBoard[7][6] = new Knight('N', Color.WHITE, 'g', 1);
		chessBoard[7][7] = new Rook('R', Color.WHITE, 'h', 1);
		kingWhite = chessBoard[7][4];
		kingBlack = chessBoard[0][4];
	}
	
	/**
	 * Converts file to 2D array column value.
	 * char to int conversion method.
	 * 
	 * @param file	char that represents the column of an actual chess board
	 * @return 2D array column
	 */
	public static int fileToCol(char file) {
		return file - 'a';
	}
	
	/**
	 * Converts rank to 2D array row value.
	 * Array row values increases from top down, whilst chess row values increase from bottom-up.
	 * 
	 * @param rank	char that represents the row of an actual chess board
	 * @return 2D array row value
	 */
	public static int rankToRow(Integer rank) {
		return 8 - rank;
	}
	
	/**
	 * Converts row to rank value.
	 * Array row values increases from top down, whilst chess row values increase from bottom-up.
	 * 
	 * @param row	2D array row value
	 * @return chess rank value
	 */
	public static int rowToRank(int row) {
		// flipped
		return rankToRow(row);
	}
	
	/**
	 * Converts col to file value.
	 * int to char conversion method
	 * 
	 * @param col	2D array column value
	 * @return chess file value
	 */
	public static char colToFile(int col) {
		return (char)('a' + col);
	}
	
	/**
	 * Alternates the current turn between white and black
	 * 
	 * @see Color
	 */
	public void switchColorMove() {
		if(colorMove == Color.BLACK) {
			colorMove = Color.WHITE;}
		else {colorMove = Color.BLACK;}
	}
	
	/**
	 * Returns boolean of whether a king, with the color kingColor, is in check.
	 * Iterates through the chessBoard array to see if any of the enemy chess pieces are 
	 * able to take the king (with the color kingColor).
	 * 
	 * @param kingColor	Color object that indicates what color the king is
	 * @return checkBoolean boolean that indicates whether the king is in check
	 * @see Color
	 * @see Piece
	 */
	public boolean kingInCheck(Color kingColor) {
		if (kingColor == Color.WHITE) {
			ArrayList<Integer> whiteKingCoordinates = new ArrayList<>(Arrays.asList(
					kingWhite.getCoordinates()[0],
					kingWhite.getCoordinates()[1]));
			if (getAllMoves(Color.BLACK).contains(whiteKingCoordinates)) {
				return true;
			}
		} else {
			ArrayList<Integer> blackKingCoordinates = new ArrayList<>(Arrays.asList(
					kingBlack.getCoordinates()[0],
					kingBlack.getCoordinates()[1]));
			if (getAllMoves(Color.WHITE).contains(blackKingCoordinates)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets all of the moves of pieces with provided color
	 * @param color
	 * @return array of coordinates that are possible moves
	 */
	public ArrayList<ArrayList<Integer>> getAllMoves(Color color) {
		ArrayList<ArrayList<Integer>> moves = new ArrayList<>();

		for (int r = 0; r < 8; r++) {
			for (int c = 0; c < 8; c++) {
				if (chessBoard[r][c] != null && chessBoard[r][c].getColor() == color) {
					moves.addAll(chessBoard[r][c].getValidPositions(this));
				}
			}
		}

		return moves;
	}

	/**
	 * Checks whether king is in checkmate by checking all opponent moves and if king's coords are in one of the moves
	 * @param kingColor
	 * @return
	 */
	public boolean kingInCheckmate(Color kingColor) {
		for (int r = 0; r < 8; r++) {
			for (int c = 0; c < 8; c++) {
				Piece piece = chessBoard[r][c];
				if (piece != null && kingColor == piece.getColor()) {
					int fromCoords[] = piece.getCoordinates();
					ArrayList<ArrayList<Integer>> pieceMoves = piece.getValidPositions(this);
					for (ArrayList<Integer> pieceMove : pieceMoves) {
						int toCoords[] = new int[]{pieceMove.get(0), pieceMove.get(1)};

						// Saves about to be taken piece
						Piece takenPiece = chessBoard[toCoords[0]][toCoords[1]];

						if (piece instanceof Pawn) {
							// En passant condition
							if (
									fromCoords[1] != toCoords[1] // captures diagonally
											&& chessBoard[fromCoords[0]][toCoords[1]] != null // piece next to it is not null
											&& chessBoard[fromCoords[0]][toCoords[1]] instanceof Pawn // piece next to it is pawn
											&& ((Pawn)chessBoard[fromCoords[0]][toCoords[1]]).isEnPassant()
							) {
								// Updates piece to the one next to it rather than the space it actually takes
								takenPiece = chessBoard[fromCoords[0]][toCoords[1]];

								// Removes enpassant pawn next to the playing piece
								setBoard(fromCoords[0], toCoords[1], null);
							}
						}

						setBoard(fromCoords[0], fromCoords[1], null);
						piece.setRank(Board.rowToRank(toCoords[0]));
						piece.setFile(Board.colToFile(toCoords[1]));
						piece.setCoordinates(toCoords);
						setBoard(toCoords[0], toCoords[1], piece);

						if (!kingInCheck(kingColor)) {
							// Reverts piece
							setBoard(fromCoords[0], fromCoords[1], piece);

							piece.setRank(Board.rowToRank(fromCoords[0]));
							piece.setFile(Board.colToFile(fromCoords[1]));
							piece.setCoordinates(fromCoords);

							setBoard(toCoords[0], toCoords[1], null);
							if (takenPiece != null) {
								setBoard(takenPiece.getCoordinates()[0], takenPiece.getCoordinates()[1], takenPiece);
							}
							return false;
						}

						// Reverts piece
						setBoard(fromCoords[0], fromCoords[1], piece);

						piece.setRank(Board.rowToRank(fromCoords[0]));
						piece.setFile(Board.colToFile(fromCoords[1]));
						piece.setCoordinates(fromCoords);

						setBoard(toCoords[0], toCoords[1], null);
						if (takenPiece != null) {
							setBoard(takenPiece.getCoordinates()[0], takenPiece.getCoordinates()[1], takenPiece);
						}
					}

				}
			}
		}
		return true;
	}
	
	/**
	 * Places piece in chessBoard[row][col].
	 * 
	 * @param row	Indicates at what row to store the piece
	 * @param col	Indicates at what col to store the piece
	 * @see Piece
	 */
	public void setBoard(int row, int col, Piece piece){
		chessBoard[row][col] = piece;
	}
	
	/**
	 * Extracts chess board coordinates from input.
	 * coords is an int[] array of size 2.
	 * coords stores row in index 0 and stores column in index 1.
	 * 
	 * @param input	String of coordinate pair from file input (Ex: "e2")
	 * @return coordinates array
	 */
	public static int[] getCoordinates(String input){
		int[] coords = new int[2];
		char file = input.charAt(0);
		coords[1] = fileToCol(file);
		String rank = input.substring(1, 2);
		coords[0] = rankToRow(Integer.parseInt(rank));
		return coords;
	}

	/**
	 * If the player does not take the opportunity to make an Enpassant move 
	 * and instead makes a different move, 
	 * this method removes the pawn's ability to make an Enpassant move.
	 * 
	 * @param color
	 */
	public void clearEnpassants(Color color) {
		for (int r = 0; r < 8; r++) {
			for (int c = 0; c < 8; c++) {
				if (chessBoard[r][c] != null
				&& chessBoard[r][c].getColor() == color
				&& chessBoard[r][c] instanceof Pawn) {
					((Pawn)chessBoard[r][c]).setEnpassant(false);
				}
			}
		}
	}

	/**
	 * Returns the chess board.
	 * 
	 * @return chessBoard 
	 * @see Piece
	 */
	public Piece[][] getBoard() {
		return chessBoard;
	}
	/**
	 * Returns the black King.
	 * 
	 * @return kingBlack 
	 * @see Piece
	 */
	public Piece getKingBlack() {
		return kingBlack;
	}
	/**
	 * Returns the white King.
	 * 
	 * @return kingWhite 
	 * @see Piece
	 */
	public Piece getKingWhite() {
		return kingWhite;
	}
	/**
	 * Returns the color of the current turn.
	 * 
	 * @return colorMove
	 * @see Piece
	 */
	public Color getColorMove() {
		return colorMove;
	}
	/**
	 * Returns a string that displays the chess board with ascii art.
	 * Pieces are represented with a color letter (b for black and w for white) and with
	 * a name letter (R for Rook, N for Knight, ...).
	 * "##" represents an empty black tile.
	 * "  " represents an empty white tile.
	 * 
	 * @return string that displays chess board
	 */
	public String printBoard() {
		String str = "";
		for(int incR = 0; incR <= 7; incR++) {
			for(int incC = 0; incC <= 7; incC++) {
				if(chessBoard[incR][incC] == null) {
					int colorCheck = (incR + incC) % 2;
					if(colorCheck == 0) {
						str = str + "   ";}
					else {str = str + "## ";}
				}
				if(chessBoard[incR][incC] != null) {
					str = str + chessBoard[incR][incC].toString();}
			}
			str = str + rowToRank(incR);
			str = str + "\n";
		}
		return str + " a  b  c  d  e  f  g  h";
	}
}
