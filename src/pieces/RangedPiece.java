package pieces;

import board.Board;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Provides moves for pieces that move long distances across the board
 * @author Kenny Salanga, Kelli Jose
 */
public interface RangedPiece {

    /**
     * Recursively Moves across the board given a rowCount and colCount direction
     *
     * @param row starting row position of piece
     * @param col starting col position of piece
     * @param rowCount moves piece across rows by rowCount amount
     * @param colCount moves piece across columns by rowCount amount
     * @param color color of piece
     * @param board provided board to see what pieces are on the indexes
     * @param validPositions validPositions arraylist that will be modified if a valid position is found
     */
    default void moveAcross(int row, int col, int rowCount, int colCount, Color color, Board board, ArrayList<ArrayList<Integer>> validPositions) {
        if (row < 0 || row > 7 || col < 0 || col > 7) return;

        if (board.getBoard()[row][col] == null) {
            validPositions.add(new ArrayList<>(Arrays.asList(row, col)));
        } else if (color == Color.WHITE) {
            if (board.getBoard()[row][col].getColor() == Color.BLACK) {
                validPositions.add(new ArrayList<>(Arrays.asList(row, col)));
            }
            return;
        } else {
            if (board.getBoard()[row][col].getColor() == Color.WHITE) {
                validPositions.add(new ArrayList<>(Arrays.asList(row, col)));
            }
            return;
        }

        moveAcross(row + rowCount, col + colCount, rowCount, colCount, color, board, validPositions);
    }
}
