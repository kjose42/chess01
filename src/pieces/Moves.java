package pieces;

import board.Board;

import java.util.ArrayList;
/**
* Moves contain methods used by all chess pieces.
* Moves' methods contribute to moving a chess piece.
* 
* @author Kenny Salanga, Kelli Jose
*/
public interface Moves {
	/**
	 * Indicates whether the piece's move is valid.
	 * input contains coordinates that the piece is moving to.
	 * Calls getValidPositions and checks if input is in the ArrayList that getValidPosition() returns.
	 * 
	 * @param input	Coordinates, taken from file input, that show where the piece is moving to (Ex: "e2")
	 * @param board	Represents chess board
	 * @return boolean	that tells whether the move is valid
	 * @see getValidPositions
	 */

    boolean move(String input, Board board);
    /**
     * Returns an ArrayList that contains coordinates that the piece can move to.
     * In the ArrayList, the coordinates are represented by Integer ArrayList.
     * 
     * @param board	Represents chess board
     * @return ArrayList that contains coordinates that the piece can move to
     */
    ArrayList<ArrayList<Integer>> getValidPositions(Board board);
    
    /**
     * Returns the piece's row in an actual chess board.
     * 
     * @return rank
     */
    int getRank();
    
    /**
     * Returns the piece's column in an actual chess board.
     * 
     * @return file
     */
    char getFile();
    
    /**
     * Returns the piece's color.
     * 
     * @return color
     */
    Color getColor();
    
    /**
     * Returns the piece's name.
     * 
     * @return name
     */
    char getName();
    
    /**
     * rank is set to be the piece's row in an actual chess board
     * 
     * @param rank 
     */
    void setRank(int rank);

    /**
     * file is set to be the piece's column in an actual chess board
     * 
     * @param file
     */
    void setFile(char file);

    /**
     * color is set to be piece's color
     * 
     * @param color
     */
    void setColor(Color color);

    /**
     * Returns current coordinates of the piece.
     * Returning an array that has row in index 0 and column in index 1.
     * 
     * @return piece's current coordinates
     */
    int[] getCoordinates();

    /**
     * coords is set to be the piece's coordinates.
     * coords is an array that has row in index 0 and column in index 1.
     * 
     * @param coords
     */
    void setCoordinates(int[] coords);
}
