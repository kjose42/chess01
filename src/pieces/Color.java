package pieces;
/** 
* Color used to differentiate white pieces from black pieces.
* @author Kenny Salanga, Kelli Jose
*/
public enum Color {
    WHITE, BLACK
}
