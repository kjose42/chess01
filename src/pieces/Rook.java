package pieces;

import java.util.ArrayList;
import java.util.Arrays;

import board.Board;

/**
 * Piece that moves straight across the board
 * can castle with the king if it hasn't moved
 * @author Kenny Salanga, Kelli Jose
 */
public class Rook extends Piece implements RangedPiece{
    /**
     * indicates whether Rook has moved for the first time
     */
    private boolean firstMove;

    /**
     * Initializes rook
     * @param name
     * @param color
     * @param file
     * @param rank
     */
    public Rook(char name, Color color, char file, int rank) {
        super(name, color, file, rank);
        firstMove = true;
    }

    /**
     * returns whether rook has made first move or not
     * @return
     */
	public boolean isFirstMove() {
		return firstMove;
	}

    /**
     * sets first move of the rook
     * @param flag
     */
	public void setFirstMove(boolean flag) {
		firstMove = flag;
	}

    /**
     * Indicates whether the piece's move is valid.
     * input contains coordinates that the piece is moving to.
     * Calls getValidPositions and checks if input is in the ArrayList that getValidPosition() returns.
     *
     * @param input	Coordinates, taken from file input, that show where the piece is moving to (Ex: "e2")
     * @param board	Represents chess board
     * @return boolean	that tells whether the move is valid
     * @see getValidPositions
     */
	@Override
    public boolean move(String input, Board board) {
    	int[] coordinates = Board.getCoordinates(input);
        int row = coordinates[0];
        int col = coordinates[1];
    	if (getValidPositions(board).contains(new ArrayList<>(Arrays.asList(row, col)))) {
    		return true;
        }
        return false;
    }


    /**
     * Returns an ArrayList that contains coordinates that the piece can move to.
     * In the ArrayList, the coordinates are represented by Integer ArrayList.
     * Also calculates if castling is valid or not
     *
     * @param board	Represents chess board
     * @return ArrayList that contains coordinates that the piece can move to
     */
    @Override
    public ArrayList<ArrayList<Integer>> getValidPositions(Board board) {//check
		int row = getCoordinates()[0];
		int col = getCoordinates()[1];

		ArrayList<ArrayList<Integer>> validPositions = new ArrayList<>();
		// Straight
		moveAcross(row + 1, col,1, 0, getColor(), board, validPositions);
		moveAcross(row - 1, col, -1, 0, getColor(), board, validPositions);
		moveAcross(row, col + 1,0, 1, getColor(), board, validPositions);
		moveAcross(row, col - 1,0, -1, getColor(), board, validPositions);

        return validPositions;
    }

    /**
     * gets Rank (row) of piece
     * @return rank
     */
    @Override
    public int getRank() {
        return this.rank;
    }

    /**
     * gets file (column) of piece
     * @return file
     */
    @Override
    public char getFile() {
        return this.file;
    }

    /**
     * gets color of piece
     * @return color
     */
    @Override
    public Color getColor() {
        return this.color;
    }

    /**
     * gets name of piece
     * @return name
     */
    @Override
    public char getName() {
        return this.name;
    }

    /**
     * gets int array coordinates of piece
     * @return coords
     */
    @Override
    public int[] getCoordinates() {
        return this.coords;
    }

    /**
     * sets rank (row) of piece
     * @param rank
     */
    @Override
    public void setRank(int rank) {
        this.rank = rank;
    }

    /**
     * sets file (column) of piece
     * @param file
     */
    @Override
    public void setFile(char file) {
        this.file = file;
    }

    /**
     * sets color ofp iece
     * @param color
     */
    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    /**
     * sets int array coordinates of piece
     * @param coords
     */
    @Override
    public void setCoordinates(int[] coords) {
        this.coords = coords;
    }

    /**
     * representation of piece in string form
     * @return
     */
    public String toString() {
        String str = "";
        if(this.color == Color.BLACK) {
            str = str + "b";}
        if(this.color == Color.WHITE) {
            str = str + "w";}
        return str + Character.toString(this.name) + " ";
    }
}
