package pieces;

import board.Board;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Piece that moves one step or two if it is its first move.
 * Can capture other pawns that made a two step move via enpassant
 *
 * @author Kenny Salanga, Kelli Jose
 */
public class Pawn extends Piece {
    /**
     * shows whether pawn has first moved or not
     */
    private boolean firstMove;
    /**
     * shows whether pawn is in an enpassant state
     */
    private boolean enPassant;

    /**
     * Initializes pawn piece
     * @param name
     * @param color
     * @param file
     * @param rank
     */
    public Pawn(char name, Color color, char file, int rank) {
        super(name, color, file, rank);
        firstMove = true;
        enPassant = false;
    }

    /**
     * returns firstMove field
     * @return
     */
    public boolean isFirstMove() { return firstMove; }

    /**
     * sets firstMove field
     * @param flag
     */
    public void setFirstMove(boolean flag) {
        firstMove = flag;
    }

    /**
     * returns if Enpassant or not
     * @return enPassant
     */
    public boolean isEnPassant() {
        return enPassant;
    }

    /**
     * sets Enpassant field
     * @param flag
     */
    public void setEnpassant(boolean flag) {
        enPassant = flag;
    }

    /**
     * Indicates whether the piece's move is valid.
     * input contains coordinates that the piece is moving to.
     * Calls getValidPositions and checks if input is in the ArrayList that getValidPosition() returns.
     *
     * @param input	Coordinates, taken from file input, that show where the piece is moving to (Ex: "e2")
     * @param board	Represents chess board
     * @return boolean	that tells whether the move is valid
     * @see getValidPositions
     */
    @Override
    public boolean move(String input, Board board) {
        String[] inputs = input.split(" ");

        int[] coordinates = Board.getCoordinates(inputs[0]);
        int inputRow = coordinates[0];
        int inputCol = coordinates[1];

        ArrayList<ArrayList<Integer>> validPositions = getValidPositions(board);
        ArrayList<Integer> toPosition = new ArrayList<>(Arrays.asList(inputRow, inputCol));

        if (validPositions.contains(toPosition)) {
            return true;
        }
        return false;
    }

    /**
     * Returns an ArrayList that contains coordinates that the piece can move to.
     * In the ArrayList, the coordinates are represented by Integer ArrayList.
     * Also calculates if castling is valid or not
     *
     * @param board	Represents chess board
     * @return ArrayList that contains coordinates that the piece can move to
     */
    @Override
    public ArrayList<ArrayList<Integer>> getValidPositions(Board board) {
        ArrayList<ArrayList<Integer>> validPositions = new ArrayList<>();
        int row = getCoordinates()[0];
        int col = getCoordinates()[1];
        if (firstMove) {
            if (getColor() == Color.WHITE) {
                if (board.getBoard()[row - 2][col] == null) {
                    validPositions.add(new ArrayList<>(Arrays.asList(row - 2, col)));
                }
            } else {
                if (board.getBoard()[row + 2][col] == null) {
                    validPositions.add(new ArrayList<>(Arrays.asList(row + 2, col)));
                }
            }
        }

        if (getColor() == Color.WHITE) {
            if (board.getBoard()[row - 1][col] == null) {
                validPositions.add(new ArrayList<>(Arrays.asList(row - 1, col)));
            }
            if (col - 1 >= 0) {
                Piece p = board.getBoard()[row - 1][col - 1];
                if (p != null && p.getColor() == Color.BLACK) {
                    validPositions.add(new ArrayList<>(Arrays.asList(row - 1, col - 1)));
                } else if ( // white enpassant left
                        board.getBoard()[row][col - 1] != null
                        && board.getBoard()[row][col - 1].getColor() == Color.BLACK
                        && board.getBoard()[row][col - 1] instanceof Pawn
                        && ((Pawn)board.getBoard()[row][col - 1]).isEnPassant()
                ) {
                    validPositions.add(new ArrayList<>(Arrays.asList(row - 1, col - 1)));
                }
            }
            if (col + 1 < 8) {
                Piece p = board.getBoard()[row - 1][col + 1];
                if (p != null && p.getColor() == Color.BLACK) {
                    validPositions.add(new ArrayList<>(Arrays.asList(row - 1, col + 1)));
                } else if ( // white enpassant right
                        board.getBoard()[row][col + 1] != null
                        && board.getBoard()[row][col + 1].getColor() == Color.BLACK
                        && board.getBoard()[row][col + 1] instanceof Pawn
                        && ((Pawn)board.getBoard()[row][col + 1]).isEnPassant()
                ) {
                    validPositions.add(new ArrayList<>(Arrays.asList(row - 1, col + 1)));
                }
            }
        } else {
            if (board.getBoard()[row + 1][col] == null) {
                validPositions.add(new ArrayList<>(Arrays.asList(row + 1, col)));
            }
            if (col - 1 >= 0) {
                Piece p = board.getBoard()[row + 1][col - 1];
                if (p != null && p.getColor() == Color.WHITE) {
                    validPositions.add(new ArrayList<>(Arrays.asList(row + 1, col - 1)));
                }
                else if ( // black enpassant left
                        board.getBoard()[row][col - 1] != null
                        && board.getBoard()[row][col - 1].getColor() == Color.WHITE
                        && board.getBoard()[row][col - 1] instanceof Pawn
                        && ((Pawn)board.getBoard()[row][col - 1]).isEnPassant()
                ) {
                    validPositions.add(new ArrayList<>(Arrays.asList(row + 1, col - 1)));
                }
            }
            if (col + 1 < 8) {
                Piece p = board.getBoard()[row + 1][col + 1];
                if (p != null && p.getColor() == Color.WHITE) {
                    validPositions.add(new ArrayList<>(Arrays.asList(row + 1, col + 1)));
                } else if ( // black enpassant right
                        board.getBoard()[row][col + 1] != null
                        && board.getBoard()[row][col + 1].getColor() == Color.WHITE
                        && board.getBoard()[row][col + 1] instanceof Pawn
                        && ((Pawn)board.getBoard()[row][col + 1]).isEnPassant()
                ) {
                    validPositions.add(new ArrayList<>(Arrays.asList(row + 1, col + 1)));
                }
            }
        }
        return validPositions;
    }

    /**
     * gets Rank (row) of piece
     * @return rank
     */
    @Override
    public int getRank() {
        return this.rank;
    }

    /**
     * gets file (column) of piece
     * @return file
     */
    @Override
    public char getFile() {
        return this.file;
    }

    /**
     * gets color of piece
     * @return color
     */
    @Override
    public Color getColor() {
        return this.color;
    }

    /**
     * gets name of piece
     * @return name
     */
    @Override
    public char getName() {
        return this.name;
    }

    /**
     * gets int array coordinates of piece
     * @return coords
     */
    @Override
    public int[] getCoordinates() {
        return this.coords;
    }

    /**
     * sets rank (row) of piece
     * @param rank
     */
    @Override
    public void setRank(int rank) {
    	this.rank = rank;
    }

    /**
     * sets file (column) of piece
     * @param file
     */
    @Override
    public void setFile(char file) {
    	this.file = file;
    }

    /**
     * sets color ofp iece
     * @param color
     */
    @Override
    public void setColor(Color color) {
    	this.color = color;
    }

    /**
     * sets int array coordinates of piece
     * @param coords
     */
    @Override
    public void setCoordinates(int[] coords) {
        this.coords = coords;
    }

    /**
     * representation of piece in string form
     * @return
     */
    public String toString() {
    	String str = "";
    	if(this.color == Color.BLACK) {
    		str = str + "b";}
    	if(this.color == Color.WHITE) {
    		str = str + "w";}
    	return str + Character.toString(this.name) + " ";
    }
}
