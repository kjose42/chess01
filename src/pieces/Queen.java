package pieces;

import board.Board;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Piece that moves straight and diagonally across the board
 *
 * @author Kenny Salanga, Kelli Jose
 */
public class Queen extends Piece implements RangedPiece {
    /**
     * initializes queen piece
     * @param name
     * @param color
     * @param file
     * @param rank
     */
    public Queen(char name, Color color, char file, int rank) {
        super(name, color, file, rank);
    }

    /**
     * Indicates whether the piece's move is valid.
     * input contains coordinates that the piece is moving to.
     * Calls getValidPositions and checks if input is in the ArrayList that getValidPosition() returns.
     *
     * @param input	Coordinates, taken from file input, that show where the piece is moving to (Ex: "e2")
     * @param board	Represents chess board
     * @return boolean	that tells whether the move is valid
     * @see getValidPositions
     */
    @Override
    public boolean move(String input, Board board) {
        int[] coordinates = Board.getCoordinates(input);
        int row = coordinates[0];
        int col = coordinates[1];
        if (getValidPositions(board).contains(new ArrayList<>(Arrays.asList(row, col)))) {
            return true;
        }
        return false;
    }

    /**
     * Returns an ArrayList that contains coordinates that the piece can move to.
     * In the ArrayList, the coordinates are represented by Integer ArrayList.
     * Also calculates if castling is valid or not
     *
     * @param board	Represents chess board
     * @return ArrayList that contains coordinates that the piece can move to
     */
    @Override
    public ArrayList<ArrayList<Integer>> getValidPositions(Board board) {
        int row = getCoordinates()[0];
        int col = getCoordinates()[1];

        // Diagonal
        ArrayList<ArrayList<Integer>> validPositions = new ArrayList<>();
        moveAcross(row + 1, col + 1,1, 1, getColor(), board, validPositions);
        moveAcross(row + 1, col - 1, 1, -1, getColor(), board,validPositions);
        moveAcross(row - 1, col + 1,-1, 1, getColor(), board, validPositions);
        moveAcross(row - 1, col - 1,-1, -1, getColor(), board, validPositions);

        // Straight
        moveAcross(row + 1, col,1, 0, getColor(), board, validPositions);
        moveAcross(row - 1, col, -1, 0, getColor(), board,validPositions);
        moveAcross(row, col + 1,0, 1, getColor(), board, validPositions);
        moveAcross(row, col - 1,0, -1, getColor(), board, validPositions);

        return validPositions;
    }
    /**
     * gets Rank (row) of piece
     * @return rank
     */
    @Override
    public int getRank() {
        return this.rank;
    }

    /**
     * gets file (column) of piece
     * @return file
     */
    @Override
    public char getFile() {
        return this.file;
    }

    /**
     * gets color of piece
     * @return color
     */
    @Override
    public Color getColor() {
        return this.color;
    }

    /**
     * gets name of piece
     * @return name
     */
    @Override
    public char getName() {
        return this.name;
    }

    /**
     * gets int array coordinates of piece
     * @return coords
     */
    @Override
    public int[] getCoordinates() {
        return this.coords;
    }

    /**
     * sets rank (row) of piece
     * @param rank
     */
    @Override
    public void setRank(int rank) {
        this.rank = rank;
    }

    /**
     * sets file (column) of piece
     * @param file
     */
    @Override
    public void setFile(char file) {
        this.file = file;
    }

    /**
     * sets color ofp iece
     * @param color
     */
    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    /**
     * sets int array coordinates of piece
     * @param coords
     */
    @Override
    public void setCoordinates(int[] coords) {
        this.coords = coords;
    }

    /**
     * representation of piece in string form
     * @return
     */
    public String toString() {
        String str = "";
        if(this.color == Color.BLACK) {
            str = str + "b";}
        if(this.color == Color.WHITE) {
            str = str + "w";}
        return str + Character.toString(this.name) + " ";
    }
}
