package pieces;

import board.Board;

import java.util.ArrayList;
/**
* Piece object is a class extended to all chess pieces.
* All chess pieces have: rank, file, name, and color.
* 
* @author Kenny Salanga, Kelli Jose
*/
public abstract class Piece implements Moves {
    
    /**
    * Piece's row location in an actual chess board.
    */
    protected int rank;
    
    /**
    * Piece's column location in an actual chess board.
    */
    protected char file;
    
    /**
    * Piece's name.
    * R = Rook
    * N = Knight
    * B = Bishop
    * Q = Queen
    * K = King
    * p = pawn
    */
    protected char name;
    
    /**
    * Piece's color.
    * (can either be black or white)
    */
    protected Color color;
    
    /**
    * Piece's coordinates.
    * coords [0] = row.
    * coords[1] = col.
    */
    protected int[] coords;
    
    /**
    * Initializes Piece object
    *
    * @param name
    * @param color
    * @param file
    * @param rank
    */
    Piece(char name, Color color, char file, int rank) {
        this.name = name;
        this.color = color;
        this.file = file;
        this.rank = rank;
        this.coords = Board.getCoordinates(file + Integer.toString(rank));
    }
}