package pieces;
/**
* Bishop is a Piece that moves diagonally
* 
* @author Kenny Salanga, Kelli Jose
*/
import java.util.ArrayList;
import java.util.Arrays;

import board.Board;

public class Knight extends Piece {
	/**
	 * Initializes Knight object
	 * 
	 * @param name
	 * @param color
	 * @param file
	 * @param rank
	 */
    public Knight(char name, Color color, char file, int rank) {
        super(name, color, file, rank);
    }
    
    /**
	 * Indicates whether the Knight's move is valid.
	 * input contains coordinates that the Knight is moving to.
	 * Calls getValidPositions and checks if input is in the ArrayList that getValidPosition() returns.
	 * 
	 * @param input	Coordinates, taken from file input, that show where the piece is moving to (Ex: "e2")
	 * @param board	Represents chess board
	 * @return boolean	that tells whether the move is valid
	 * @see getValidPositions
	 */
    @Override
    public boolean move(String input, Board board) {
    	int[] coordinates = Board.getCoordinates(input);
        int row = coordinates[0];
        int col = coordinates[1];
        ArrayList<ArrayList<Integer>> validPos = getValidPositions(board);
    	if (validPos.contains(new ArrayList<>(Arrays.asList(row, col)))) {
            return true;
        }
        return false;
    }

    /**
     * Returns an ArrayList that contains coordinates that the Knight can move to.
     * In the ArrayList, the coordinates are represented by Integer ArrayList.
     * 
     * @param board	Represents chess board
     * @return ArrayList that contains coordinates that the Knight can move to
     */
    @Override
    public ArrayList<ArrayList<Integer>> getValidPositions(Board board) {
    	ArrayList<ArrayList<Integer>> validPos = new ArrayList<ArrayList<Integer>>();
    	int row = this.coords[0];
    	int col= this.coords[1];
    	if((row - 1) >= 0 && (col - 2) >= 0) {
    		if(board.getBoard()[row - 1][col - 2] == null
			|| (board.getBoard()[row - 1][col - 2] != null && board.getBoard()[row - 1][col - 2].getColor() != this.getColor())) {
    			ArrayList<Integer> addCoords = new ArrayList<Integer>();
    			addCoords.add(row - 1);
    			addCoords.add(col - 2);
    			validPos.add(addCoords);
    		}
    	}
    	if((row - 2) >= 0 && (col - 1) >= 0){
    		if(board.getBoard()[row - 2][col - 1] == null
			|| (board.getBoard()[row - 2][col - 1] != null && board.getBoard()[row - 2][col - 1].getColor() != this.getColor())) {
    			ArrayList<Integer> addCoords = new ArrayList<Integer>();
    			addCoords.add(row - 2);
    			addCoords.add(col - 1);
    			validPos.add(addCoords);
    		}
    	}
    	if((row - 2) >= 0 && (col + 1) <= 7){
    		if(board.getBoard()[row - 2][col + 1] == null
			|| (board.getBoard()[row - 2][col + 1] != null && board.getBoard()[row - 2][col + 1].getColor() != this.getColor())) {
    			ArrayList<Integer> addCoords = new ArrayList<Integer>();
    			addCoords.add(row - 2);
    			addCoords.add(col + 1);
    			validPos.add(addCoords);
    		}
    	}
    	if((row - 1) >= 0 && (col + 2) <= 7) {
    		if(board.getBoard()[row - 1][col + 2] == null) {
    			ArrayList<Integer> addCoords = new ArrayList<Integer>();
    			addCoords.add(row - 1);
    			addCoords.add(col + 2);
    			validPos.add(addCoords);
    		}
    	}
    	if((row + 1) <= 7 && (col + 2) <= 7) {
    		if(board.getBoard()[row + 1][col + 2] == null
			|| (board.getBoard()[row + 1][col + 2] != null && board.getBoard()[row + 1][col + 2].getColor() != this.getColor())) {
    			ArrayList<Integer> addCoords = new ArrayList<Integer>();
    			addCoords.add(row + 1);
    			addCoords.add(col + 2);
    			validPos.add(addCoords);
    		}
    	}
    	if((row + 2) <= 7 && (col + 1) <= 7) {
    		if(board.getBoard()[row + 2][col + 1] == null
			|| (board.getBoard()[row + 2][col + 1] != null && board.getBoard()[row + 2][col + 1].getColor() != this.getColor())) {
    			ArrayList<Integer> addCoords = new ArrayList<Integer>();
    			addCoords.add(row + 2);
    			addCoords.add(col + 1);
    			validPos.add(addCoords);
    		}
    	}
    	if((row + 2) <= 7 && (col - 1) >= 0) {
    		if(board.getBoard()[row + 2][col - 1] == null
			|| (board.getBoard()[row + 2][col - 1] != null && board.getBoard()[row + 2][col - 1].getColor() != this.getColor())) {
    			ArrayList<Integer> addCoords = new ArrayList<Integer>();
    			addCoords.add(row + 2);
    			addCoords.add(col - 1);
    			validPos.add(addCoords);
    		}
    	}
    	if((row + 1) <= 7 && (col - 2) >= 0) {
    		if(board.getBoard()[row + 1][col - 2] == null
			|| (board.getBoard()[row + 1][col - 2] != null && board.getBoard()[row + 1][col - 2].getColor() != this.getColor())) {
    			ArrayList<Integer> addCoords = new ArrayList<Integer>();
    			addCoords.add(row + 1);
    			addCoords.add(col - 2);
    			validPos.add(addCoords);
    		}
    	}
    	return validPos;
    }
    
    /**
     * Returns the Knight's row in an actual chess board.
     * 
     * @return rank
     */
    @Override
    public int getRank() {
        return this.rank;
    }
    
    /**
     * Returns the Knight's column in an actual chess board.
     * 
     * @return file
     */
    @Override
    public char getFile() {
        return this.file;
    }
    
    /**
     * Returns the Knight's color.
     * 
     * @return color
     */
    @Override
    public Color getColor() {
        return this.color;
    }

    /**
     * Returns the Knight's name.
     * 
     * @return name
     */
    @Override
    public char getName() {
        return this.name;
    }
    
    /**
     * Returns current coordinates of the Knight.
     * Returning an array that has row in index 0 and column in index 1.
     * 
     * @return Knight's current coordinates
     */
    @Override
    public int[] getCoordinates() {
        return this.coords;
    }
    
    /**
     * file is set to be the Bishop's column in an actual chess board
     * 
     * @param rank
     */
    @Override
    public void setRank(int rank) {
    	this.rank = rank;
    }

    /**
     * file is set to be the piece's column in an actual chess board
     * 
     * @param file
     */
    @Override
    public void setFile(char file) {
    	this.file = file;
    }

    /**
     * color is set to be piece's color
     * 
     * @param color
     */
    @Override
    public void setColor(Color color) {
    	this.color = color;
    }
    
    /**
     * coords is set to be the piece's coordinates.
     * coords is an array that has row in index 0 and column in index 1.
     * 
     * @param coords
     */
    @Override
    public void setCoordinates(int[] coords) {
        this.coords = coords;
    }
    
    /**
     * Returns a string that displays the Knight with ascii art.
     * Knight are represented with a color letter (b for black and w for white) and with
     * a "N" (Ex: "wB").
     * 
     * @return string that displays Knight with ascii art
     */
    public String toString() {
    	String str = "";
    	if(this.color == Color.BLACK) {
    		str = str + "b";}
    	if(this.color == Color.WHITE) {
    		str = str + "w";}
    	return str + Character.toString(this.name) + " ";
    }
}
