package pieces;

import java.util.ArrayList;
import java.util.Arrays;

import board.Board;

/**
 * Piece that moves only one spot
 * can castle and is the most important piece
 * @author Kenny Salanga, Kelli Jose
 */
public class King extends Piece{
	/**
	 * indicates whether King has moved for the first time
	 */
	private boolean firstMove;

    /**
     * this field is filled to see if any of the opposing side's moves are blocking the king from castling or putting it in check
     */
    private ArrayList<ArrayList<Integer>> enemyMoves;
    
    /**
	 * Initializes King object
	 * 
	 * @param name
	 * @param color
	 * @param file
	 * @param rank
	 */
    public King(char name, Color color, char file, int rank) {
        super(name, color, file, rank);
        firstMove = true;
    }

    /**
     * sets the enemy moves field
     * @param enemyMoves
     */
    public void setEnemyMoves(ArrayList<ArrayList<Integer>> enemyMoves) {
        this.enemyMoves = enemyMoves;
    }

    /**
     * Returns boolean of whether the King has moved for the first time
     * 
     * @return firstMove
     */
    public boolean isFirstMove() {
        return firstMove;
    }

    /**
     * Sets firstMove field
     * @param flag flag to set firstMove
     */
    public void setFirstMove(boolean flag) {
        firstMove = flag;
    }

    /**
	 * Indicates whether the piece's move is valid.
	 * input contains coordinates that the piece is moving to.
	 * Calls getValidPositions and checks if input is in the ArrayList that getValidPosition() returns.
	 * 
	 * @param input	Coordinates, taken from file input, that show where the piece is moving to (Ex: "e2")
	 * @param board	Represents chess board
	 * @return boolean	that tells whether the move is valid
	 * @see getValidPositions
	 */
    @Override
    public boolean move(String input, Board board) {
    	int[] coordinates = Board.getCoordinates(input);
        int row = coordinates[0];
        int col = coordinates[1];
        if (getValidPositions(board).contains(new ArrayList<>(Arrays.asList(row, col)))) {
            return true;
        }
        return false;
    } 
    
    /**
     * Returns an ArrayList that contains coordinates that the piece can move to.
     * In the ArrayList, the coordinates are represented by Integer ArrayList.
     * Also calculates if castling is valid or not
     * 
     * @param board	Represents chess board
     * @return ArrayList that contains coordinates that the piece can move to
     */
    @Override
    public ArrayList<ArrayList<Integer>> getValidPositions(Board board) {
    	ArrayList<ArrayList<Integer>> validPos = new ArrayList<ArrayList<Integer>>();
    	for(int incR = this.coords[0]-1; incR <= this.coords[0]+1; incR++) {
    		for(int incC = this.coords[1]-1; incC <= this.coords[1]+1; incC++) {
    			if(incR >= 0 && incR <= 7) {
    				if(incC >= 0 && incC <= 7) {
    					if(board.getBoard()[incR][incC] == null
                        || (board.getBoard()[incR][incC] != null && board.getBoard()[incR][incC].getColor() != this.getColor())) {
    						ArrayList<Integer> addCoords = new ArrayList<Integer>();
    						addCoords.add(incR);
    						addCoords.add(incC);
    						validPos.add(addCoords);
    					}
    				}
    			}
    		}
    	}

        Piece checkPiece = board.getBoard()[this.coords[0]][0];
        boolean kingInCheck = enemyMoves != null ?
                enemyMoves.contains(new ArrayList<>(Arrays.asList(getCoordinates()[0], getCoordinates()[1])))
                : false;

        // Left Side Castle Check
        if(firstMove && checkPiece instanceof Rook && !kingInCheck) { //
        	Rook rook = (Rook)board.getBoard()[this.coords[0]][0];
        	if(rook.isFirstMove()) {
        		boolean castleBlocked = false;

        		for(int inc = 1; inc <= 3; inc++) {
        			if(board.getBoard()[this.coords[0]][inc] != null) {
        				castleBlocked = true;
                        break;
                    }
                    ArrayList<Integer> CastleTile = new ArrayList<>(Arrays.asList(this.coords[0], inc));
                    if (enemyMoves != null && enemyMoves.contains(CastleTile)) {
                        castleBlocked = true;
                        break;
                    }
                }

        		if(!castleBlocked) {
        			ArrayList<Integer> addCoords = new ArrayList<Integer>();
        			addCoords.add(this.coords[0]);
        			addCoords.add(2);
        			validPos.add(addCoords);
        		}
        	}
        }

        checkPiece = board.getBoard()[this.coords[0]][7];

        // Right Side Castle Check
        if(firstMove && checkPiece instanceof Rook && !kingInCheck) { // && !kingInCheck
        	Rook rook = (Rook)board.getBoard()[this.coords[0]][7];
        	if(rook.isFirstMove()) {
        		boolean castleBlocked = false;

        		for(int inc = 5; inc <= 6; inc++) {
        			if(board.getBoard()[this.coords[0]][inc] != null) {
        				castleBlocked = true;
                        break;
                    }

                    ArrayList<Integer> CastleTile = new ArrayList<>(Arrays.asList(this.coords[0], inc));
                    if (enemyMoves != null && enemyMoves.contains(CastleTile)) {
                        castleBlocked = true;
                        break;
                    }
                }

        		if(!castleBlocked) {
        			ArrayList<Integer> addCoords = new ArrayList<Integer>();
        			addCoords.add(this.coords[0]);
        			addCoords.add(6);
        			validPos.add(addCoords);
        		}
        	}
        }
        return validPos;
    }

    /**
     * Returns the piece's row in an actual chess board.
     * 
     * @return rank
     */
    @Override
    public int getRank() {
        return this.rank;
    }

    /**
     * Returns the piece's column in an actual chess board.
     * 
     * @return file
     */
    @Override
    public char getFile() {
        return this.file;
    }

    /**
     * Returns the piece's color.
     * 
     * @return color
     */
    @Override
    public Color getColor() {
        return this.color;
    }

    /**
     *Returns the piece's name
     * @return char
     */
    @Override
    public char getName() {
        return this.name;
    }

    /**
     * gets int array coordinates of piece
     * @return coords
     */
    @Override
    public int[] getCoordinates() {
        return this.coords;
    }

    /**
     * sets rank (row) of piece
     * @param rank
     */
    @Override
    public void setRank(int rank) {
        this.rank = rank;
    }

    /**
     * sets file (column) of piece
     * @param file
     */
    @Override
    public void setFile(char file) {
        this.file = file;
    }

    /**
     * sets color ofp iece
     * @param color
     */
    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    /**
     * sets int array coordinates of piece
     * @param coords
     */
    @Override
    public void setCoordinates(int[] coords) {
        this.coords = coords;
    }

    /**
     * representation of piece in string form
     * @return
     */
    public String toString() {
        String str = "";
        if(this.color == Color.BLACK) {
            str = str + "b";}
        if(this.color == Color.WHITE) {
            str = str + "w";}
        return str + Character.toString(this.name) + " ";
    }
}
