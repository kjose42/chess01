package pieces;
/**
* Bishop is a Piece that moves diagonally
* 
* @author Kenny Salanga, Kelli Jose
*/
import board.Board;

import java.util.ArrayList;
import java.util.Arrays;

public class Bishop extends Piece implements RangedPiece {
	/**
	 * Initializes Bishop object
	 * 
	 * @param name
	 * @param color
	 * @param file
	 * @param rank
	 */
    public Bishop(char name, Color color, char file, int rank) {
        super(name, color, file, rank);
    }
    
    /**
	 * Indicates whether the Bishop's move is valid.
	 * input contains coordinates that the Bishop is moving to.
	 * Calls getValidPositions and checks if input is in the ArrayList that getValidPosition() returns.
	 * 
	 * @param input	Coordinates, taken from file input, that show where the piece is moving to (Ex: "e2")
	 * @param board	Represents chess board
	 * @return boolean	that tells whether the move is valid
	 * @see getValidPositions
	 */
    @Override
    public boolean move(String input, Board board) {
        int[] coordinates = Board.getCoordinates(input);
        int row = coordinates[0];
        int col = coordinates[1];
        ArrayList<ArrayList<Integer>> validPositions = getValidPositions(board);
        ArrayList<Integer> toPosition = new ArrayList<>(Arrays.asList(row, col));

        if (validPositions.contains(toPosition)) {
            return true;
        }
        return false;
    }

    /**
     * Returns an ArrayList that contains coordinates that the Bishop can move to.
     * In the ArrayList, the coordinates are represented by Integer ArrayList.
     * 
     * @param board	Represents chess board
     * @return ArrayList that contains coordinates that the Bishop can move to
     */
    @Override
    public ArrayList<ArrayList<Integer>> getValidPositions(Board board) {
        int place[] = getCoordinates();
        int row = place[0];
        int col = place[1];
        ArrayList<ArrayList<Integer>> validPositions = new ArrayList<>();
        moveAcross(row + 1, col + 1,1, 1, getColor(), board, validPositions);
        moveAcross(row + 1, col - 1, 1, -1, getColor(), board,validPositions);
        moveAcross(row - 1, col + 1,-1, 1, getColor(), board, validPositions);
        moveAcross(row - 1, col - 1,-1, -1, getColor(), board, validPositions);
        return validPositions;
    }
    
    /**
     * Returns the Bishop's row in an actual chess board.
     * 
     * @return rank
     */
    @Override
    public int getRank() {
        return this.rank;
    }
    
    /**
     * Returns the Bishop's column in an actual chess board.
     * 
     * @return file
     */
    @Override
    public char getFile() {
        return this.file;
    }
    
    /**
     * Returns the Bishop's color.
     * 
     * @return color
     */
    @Override
    public Color getColor() {
        return this.color;
    }

    /**
     * Returns the Bishop's name.
     * 
     * @return name
     */
    @Override
    public char getName() {
        return this.name;
    }
    
    /**
     * Returns current coordinates of the Bishop.
     * Returning an array that has row in index 0 and column in index 1.
     * 
     * @return Bishop's current coordinates
     */
    @Override
    public int[] getCoordinates() {
        return this.coords;
    }

    /**
     * file is set to be the Bishop's column in an actual chess board
     * 
     * @param rank
     */
    @Override
    public void setRank(int rank) {
    	this.rank = rank;
    }
    
    /**
     * file is set to be the Bishop's column in an actual chess board
     * 
     * @param file
     */
    @Override
    public void setFile(char file) {
    	this.file = file;
    }

    /**
     * color is set to be Bishop's color
     * 
     * @param color
     */
    @Override
    public void setColor(Color color) {
    	this.color = color;
    }
    
    /**
     * coords is set to be the piece's coordinates.
     * coords is an array that has row in index 0 and column in index 1.
     * 
     * @param coords
     */
    @Override
    public void setCoordinates(int[] coords) {
        this.coords = coords;
    }
    
    /**
     * Returns a string that displays the Bishop with ascii art.
     * Bishops are represented with a color letter (b for black and w for white) and with
     * a "B" (Ex: "wB").
     * 
     * @return string that displays Bishop with ascii art
     */
    public String toString() {
    	String str = "";
    	if(this.color == Color.BLACK) {
    		str = str + "b";}
    	if(this.color == Color.WHITE) {
    		str = str + "w";}
    	return str + Character.toString(this.name) + " ";
    }
}
